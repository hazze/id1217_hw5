The algorithm works so the first process sends its array content to the second process one index at a time. If the value matches any of the values in the second process' array it will pass this value back to the first process. If the values match for the first process it will send the value to the third else it will send -1. If I didn't send a -1 the third process would cause an bug and the program would crash.

The third process check its array and returns the value if found or -1 if not. If a match is found the first process prints it and increments an int that counts number of total matches.

On every received message the second and third process will loop through their entire array until they find a match. This is to check if the value sent from process 1 is located on another index than in the first array.


Compile: mpicc welfare.c -o welfare
Run: mpiexec -np 3 welfare
