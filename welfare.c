#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>

#include <mpi.h>

#define ARRAY_SIZE 20

int result;
int msg1to2, msg2to1, msg1to3, msg3to1;

int run = 1;

int * generate(int start) {
  static int array[ARRAY_SIZE];
  int i;
  for (i = 0; i < ARRAY_SIZE; i++) {
    array[i] = i + start;
  }
  array[4] = 1337;
  return array;
}

void first(int index) {
  int * f = generate(rand() % 5);

  if (run) {
    int p;
    for (p = 0; p < ARRAY_SIZE; p++)
      printf("f: %d\n", *(f + p));
    run = 0;
    printf("\n");
  }

  msg1to2 = *(f + index);
  // printf("msg1to2: %d\n", msg1to2);
  MPI_Send(&msg1to2, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
  MPI_Recv(&msg2to1, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  // printf("msg2to1: %d == *(f + index): %d\n", msg2to1, *(f + index));
  if(msg2to1 == *(f + index)) {
    msg1to3 = *(f + index);
    MPI_Send(&msg1to3, 1, MPI_INT, 2, 2, MPI_COMM_WORLD);
    MPI_Recv(&msg3to1, 1, MPI_INT, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    // printf("msg3to1: %d == *(f + index): %d\n", msg3to1, *(f + index));
    if (msg3to1 == *(f + index)) {
      printf("%d FINNS I SAMTLIGA\n", *(f + index));
      result++;
    }
  }
  else {
    msg1to3 = -1;
    MPI_Send(&msg1to3, 1, MPI_INT, 2, 2, MPI_COMM_WORLD);
    MPI_Recv(&msg3to1, 1, MPI_INT, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }
}

void second(int index) {
  int i;
  int * g = generate(10);

  if (run) {
    int p;
    for (p = 0; p < ARRAY_SIZE; p++)
      printf("g: %d\n", *(g + p));
    run = 0;
    printf("\n");
  }

  MPI_Recv(&msg1to2, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  for (i = 0; i < ARRAY_SIZE; i++) {
    // printf("*(g + i): %d, msg1to2: %d\n", *(g + i), msg1to2);
    if ((*(g + i)) == msg1to2) {
      msg2to1 = *(g + i);
      break;
    }
    else
      msg2to1 = -1;
  }
  MPI_Send(&msg2to1, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
}

void third(int index) {
  int i;
  int * h = generate(5);

  if (run) {
    int p;
    for (p = 0; p < ARRAY_SIZE; p++)
      printf("h: %d\n", *(h + p));
    run = 0;
    printf("\n");
  }

  MPI_Recv(&msg1to3, 1, MPI_INT, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  for (i = 0; i < ARRAY_SIZE; i++) {
    if ((*(h + i)) == msg1to3) {
      msg3to1 = *(h + i);
      break;
    }
    else
      msg3to1 = -1;
  }
  MPI_Send(&msg3to1, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  // Find out rank, size
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // We are assuming at least 2 processes for this task
  if (world_size < 2) {
    fprintf(stderr, "World size must be greater than 1 for %s\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  int k = 0;
  while(k < ARRAY_SIZE) {
    if (world_rank == 0) {
      first(k);
    }
    else if (world_rank == 1) {
      second(k);
    }
    else if (world_rank == 2) {
      third(k);
    }
    usleep(1000);
    k++;
  }

  // for (k = 0; k < ARRAY_SIZE; k++)
  if (world_rank == 0)
    printf("\nTotalt hits in all 3 arrays: %d\n", result);


  MPI_Finalize();
}
